#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:18976768:50c7501249dd9f4beeec9053d4aa1f5afa1fd5e1; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:16777216:49b39de24c701d75c3b88a989fa76cfa7ff6e1ac EMMC:/dev/block/bootdevice/by-name/recovery 50c7501249dd9f4beeec9053d4aa1f5afa1fd5e1 18976768 49b39de24c701d75c3b88a989fa76cfa7ff6e1ac:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
